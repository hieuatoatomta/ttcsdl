
-- Nhập dữ liệu cho từng danh mục
-- 1) Nhập dữ liệu cho bảng nhân viên
create proc insertEmployee
	@tennhanvien varchar(50),
	@sodienthoai varchar(50),
	@gioitinh bit
	as
	begin
		begin
			begin
				DECLARE @manhanvien VARCHAR(50)
				IF (SELECT COUNT(MaNV) FROM NhanVien) = 0
					SET @manhanvien = '0'
				ELSE
						SELECT @manhanvien = MAX(RIGHT(MaNV, 3)) FROM NhanVien
						SELECT @manhanvien = CASE
							WHEN @manhanvien >= 0 and @manhanvien < 9 THEN 'NV00' + CONVERT(varchar, CONVERT(INT, @manhanvien) + 1)
							WHEN @manhanvien >= 9 THEN 'NV0' + CONVERT(varchar, CONVERT(INT, @manhanvien) + 1)
						end;
			end
			begin
			if not exists (select MaNV from NhanVien where MaNV = @manhanvien)
				begin
					INSERT INTO NhanVien(MaNV,TenNV,SDT,GioiTinh) VALUES (@manhanvien, @tennhanvien,@sodienthoai,@gioitinh);
					print 'Them nhan vien thanh cong';
				end
			else
				print 'Ma nhan vien da ton tai';
			end
		end
end;

EXEC insertEmployee N'Ngo Thi Ngoc Anh', N'0981527073', 1
EXEC insertEmployee N'Thach Tho Hieu', N'0981527073', 1
EXEC insertEmployee N'Doan Xuan Hieu', N'0981527073', 1
EXEC insertEmployee N'Pham Xuan Nam', N'0981527073', 1
EXEC insertEmployee N'Tran Thai Bao', N'0981527073', 1
EXEC insertEmployee N'Thach Tho Quynh', N'0981527073', 1

-- 2) Nhập dữ liệu cho bảng nhà cung
create proc insertNhaCungCap
	@tennhacungcap varchar(50),
	@sodienthoai varchar(50),
	@diachi varchar(50)
	as
	begin
		begin
			begin
				DECLARE @manhacungcap VARCHAR(50)
				IF (SELECT COUNT(MaNCC) FROM NhaCungCap) = 0
					SET @manhacungcap = '0'
				ELSE
						SELECT @manhacungcap = MAX(RIGHT(MaNCC, 3)) FROM NhaCungCap
						SELECT @manhacungcap = CASE
							WHEN @manhacungcap >= 0 and @manhacungcap < 9 THEN 'NCC00' + CONVERT(varchar, CONVERT(INT, @manhacungcap) + 1)
							WHEN @manhacungcap >= 9 THEN 'NCC0' + CONVERT(varchar, CONVERT(INT, @manhacungcap) + 1)
						end;
			end
			begin
			if not exists (select MaNCC from NhaCungCap where MaNCC = @manhacungcap)
				begin
					INSERT INTO NhaCungCap(MaNCC,TenNCC,SDT,DiaChi) VALUES (@manhacungcap, @tennhacungcap,@sodienthoai,@diachi);
					print 'Them nha cung cap thanh cong';
				end
			else
				print 'Ma nha cung cap da ton tai';
			end
		end
end;

EXEC insertNhaCungCap N'Cong ty may mac May10', N'0981527073', N'Thach Ban, Long Bien, Ha Noi'
EXEC insertNhaCungCap N'Cong ty may mac Ato', N'0981527073', N'Dinh Xuyen, Gia Lam, Ha Noi'
EXEC insertNhaCungCap N'Cong ty may mac Adidas', N'0981527073', N'Thach Ban, Long Bien, Ha Noi'
EXEC insertNhaCungCap N'Cong ty may mac Nike', N'0981527073', N'Thach Ban, Long Bien, Ha Noi'
EXEC insertNhaCungCap N'Cong ty may mac ThoHieu', N'0981527073', N'Thach Ban, Long Bien, Ha Noi'
EXEC insertNhaCungCap N'Cong ty may mac Vans', N'0981527073', N'Thach Ban, Long Bien, Ha Noi'

-- 3) Nhập dữ liệu cho bảng loại sản

INSERT INTO LoaiSanPham( MaLoaiSP, TenLoaiSP, GhiChu )
VALUES ('LSP001', N'Ao khoac', ''),
		('LSP002', N'Ao khoac ni', ''),
		('LSP003', N'Quan bo', ''),
		('LSP004', N'Ao Phong', ''),
		('LSP005', N'Vay', '')


-- 4) Nhập dữ liệu cho bảng kho
insert into Kho(MaKho,SoLuongTon,SoLuongNhap,SoLuongXuat,SoLuongDau)
  values('K001',0,0,0,0),
		('K002',0,0,0,0),
		('K003',0,0,0,0),
		('K004',0,0,0,0),
		('K005',0,0,0,0)

-- 1) Nhập dữ liệu cho bảng sản phẩm

create proc insertProduct
	@maloaisanpham varchar(50),
	@makho varchar(50),
	@tensanpham varchar(50),
	@donvitinh varchar(50),
	@soluong int,
	@dongia float
	as
	begin
		if @maloaisanpham = (select MaLoaiSP from LoaiSanPham where MaLoaiSP = @maloaisanpham)
		begin
			if @makho = (select MaKho from Kho where MaKho = @makho)
				begin
					begin
						DECLARE @masanpham VARCHAR(50)
						IF (SELECT COUNT(MaSP) FROM SanPham) = 0
							SET @masanpham = '0'
						ELSE
								SELECT @masanpham = MAX(RIGHT(MaSP, 3)) FROM SanPham
								SELECT @masanpham = CASE
									WHEN @masanpham >= 0 and @masanpham < 9 THEN 'SP00' + CONVERT(varchar, CONVERT(INT, @masanpham) + 1)
									WHEN @masanpham >= 9 THEN 'SP0' + CONVERT(varchar, CONVERT(INT, @masanpham) + 1)
								end;
					end
						begin
						if not exists (select MaSP from SanPham where MaSP = @masanpham)
							begin
								INSERT INTO SanPham(MaSP,MaLoaiSP,MaKho,TenSP,DVT,SoLuong,DonGia) VALUES (@masanpham, @maloaisanpham,@makho,@tensanpham,@donvitinh,@soluong,@dongia);
								print 'Them san pham thanh cong';
							end
						else
							print 'Ma san pham da ton tai';
					end
				end
			else
				print 'Ma kho ' +@makho + ' khong ton tai !!'
		end
		else
			print 'Ma loai ' +@maloaisanpham + ' khong ton tai !!'
end;



EXEC insertProduct N'LSP001', N'K001', N'Ao khoac the thao', N'Chiec',10, 100000
EXEC insertProduct N'LSP001', N'K001', N'Ao khoac ni', N'Chiec',9, 120000
EXEC insertProduct N'LSP002', N'K002', N'Ao khoac den', N'Chiec',20, 160000
EXEC insertProduct N'LSP002', N'K002', N'Ao khoac sang', N'Chiec',30, 180000
EXEC insertProduct N'LSP003', N'K001', N'Quan bo dai', N'Chiec',14, 110000
EXEC insertProduct N'LSP003', N'K001', N'Quan bo den', N'Chiec',16, 90000


-- Nghiệp vụ: Xử lý hoá đơn bán ra
-- Các thuộc tính cần nhập: Mã sản phẩm, mã nhân viên, hình thức thanh toán, ngày lập, đơn vị tính, số lượng, tên khách hàng, số điện thoại, email.
--
-- Bước 1: Kiếm tra mã nhân viên có tồn tại trong bảng nhân viên hay không?
--  Nếu không có in ra thông báo: Nhân viên ( mã nhân viên) không tồn tại.
--  Nếu có tiếp tục
-- Bước 2: Kiếm tra mã sản phẩm có tồn tại trong bảng sản phẩm hay không( nếu không có in ra thông báo: Sản phẩm ( mã sản phẩm) không tồn tại
-- Bước 3: Tự sinh ra mã hoá đơn với mã khách hàng theo từng hoá đơn
-- Cú pháp: KHxx / HDxx
-- Bước 4: Kiếm tra xem mã hoá đơn và mã khách hàng đã tồn tại trong database chưa? nếu rồi in ra thông báo mã hoá đơn/ mã khách hàng đã tồn tại
-- Bước 5: Kiểm tra số lượng nhập vào có lớn hơn trong số lượng mã sản phẩm trong bảng sản phẩm.
-- + Nếu lớn hơn số lượng trong bảng sản phẩm in ra thông báo: Số lượng hàng ( mã sản phẩm) không đủ.
-- + Nếu nhỏ hơn số lượng nhập vào nhỏ hơn số lượng trong bảng sản phẩm thì cập nhật lại số lượng trong bảng sản phẩm
-- —> Nếu các bước kiểm tra đã đủ thì insert dữ liệu vào các bảng: Hoá đơn, chi tiết hoá đơn, khách hàng theo dữ liệu nhập
-- In ra thông báo: Thêm hoá đơn thành công
-- 									Thêm hoá đơn chi tiết thành công
-- 									Thêm khách hàng thành công
CREATE proc insertdata
	@manhanvien varchar(50),
	@masanpham varchar(50),
	@hinhthucthanhtoan varchar(10),
	@ngaylap varchar(20),
	@donvitinh varchar(20),
	@soluong int,
	@tenkhachhang varchar(50),
	@sodienthoai varchar(50),
	@email varchar(50)
	as
	begin
		DECLARE @dongia  float
		begin
			if @manhanvien = (select MaNV from NhanVien where MaNV =@manhanvien)
				begin
					if  @masanpham = (select MaSP from SanPham where MaSP = @masanpham)
						begin
							set @dongia = (select DonGia from SanPham where MaSP = @masanpham)
							-- sinh ma hoa don
							begin
								DECLARE @mahoadon VARCHAR(50)
								IF (SELECT COUNT(MaHD) FROM HoaDon) = 0
									SET @mahoadon = '0'
								ELSE
										SELECT @mahoadon = MAX(RIGHT(MaHD, 3)) FROM HoaDon
										SELECT @mahoadon = CASE
											WHEN @mahoadon >= 0 and @mahoadon < 9 THEN 'HD00' + CONVERT(varchar, CONVERT(INT, @mahoadon) + 1)
											WHEN @mahoadon >= 9 THEN 'HD0' + CONVERT(varchar, CONVERT(INT, @mahoadon) + 1)
										end;
							end
							begin
								begin
									DECLARE @makhachhang varchar(50)
									IF (SELECT COUNT(MaKH) FROM KhachHang) = 0
										SET @makhachhang = '0'
									ELSE
										SELECT @makhachhang = MAX(RIGHT(MaKH, 3)) FROM KhachHang
										SELECT @makhachhang = CASE
											WHEN @makhachhang >= 0 and @makhachhang < 9 THEN 'KH00' + CONVERT(varchar, CONVERT(INT, @makhachhang) + 1)
											WHEN @makhachhang >= 9 THEN 'KH0' + CONVERT(varchar, CONVERT(INT, @makhachhang) + 1)
										end;
								end
								begin
									if(@soluong < (select SoLuong from SanPham where MaSP = @masanpham))
										begin
											if not exists (select MaKH from KhachHang where MaKH = @makhachhang)
												begin
													INSERT INTO KhachHang(MaKH,TenKH,SDT,Email) VALUES ( @makhachhang,@tenkhachhang,@sodienthoai,@email);
													print 'Them khach hang thanh cong';
												end
											else
												print 'Ma khach hang da ton tai';
											if not exists (select MaHD from HoaDon where MaHD = @makhachhang)
												begin
													INSERT INTO HoaDon(MaHD,MaKH,MaNV,HinhThucThanhToan,NgayLap) VALUES ( @mahoadon,@makhachhang,@manhanvien,@hinhthucthanhtoan,@ngaylap );
													print 'Them hoa don thanh cong';
													INSERT INTO CTHoaDon(MaHD,MaSP,DVT,SoLuong,DonGia) VALUES ( @mahoadon,@masanpham,@donvitinh,@soluong,@dongia );
													print 'Them chi tiet hoa don thanh cong';
													Update SanPham set SoLuong = SoLuong - @soluong where MaSP = @masanpham;
												end
											else
												print 'Ma hoa don da ton tai';
										end
									else
										print 'So luong hang ' +@masanpham + ' khong du !';

								end
							end
						end
					else
						print 'San pham ' +  @masanpham  + ' khong ton tai';
				end
			else
			print 'Nhan vien ' +  @manhanvien  + ' khong ton tai';
		end
	end;


	EXEC insertdata N'NV001',N'SP001', N'Tien mat', N'20/12/2020', N'VND', 2,N'Thach Tho Hieu', N'0981527073', N'hieuatoato@gmail.com'
	EXEC insertdata N'NV001',N'SP002', N'Tien mat', N'22/12/2020', N'VND', 2,N'Doan Xuan Hieu', N'0981527073', N'doanxuanhieu@gmail.com'
	EXEC insertdata N'NV001',N'SP001', N'Tien mat', N'23/12/2020', N'VND', 2,N'Vu Dinh Tuyen', N'0981527073', N'vudinhtuyen@gmail.com'
	EXEC insertdata N'NV001',N'SP002', N'Tien mat', N'24/12/2020', N'VND', 2,N'Tran Tuan Anh', N'0981527073', N'trantuananh@gmail.com'
	EXEC insertdata N'NV001',N'SP004', N'Tien mat', N'25/12/2020', N'VND', 2,N'Dinh Van Luc', N'0981527073', N'dinhvanluc@gmail.com'
	EXEC insertdata N'NV001',N'SP003', N'Tien mat', N'13/12/2020', N'VND', 2,N'Pham Xuan Nam', N'0981527073', N'phamxuannam@gmail.com'
	EXEC insertdata N'NV001',N'SP005', N'Tien mat', N'27/12/2020', N'VND', 2,N'Ngo Thi Ngoc Anh', N'0981527073', N'ngothingocanh@gmail.com'
