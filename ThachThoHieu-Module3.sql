-- link git : https://gitlab.com/hieuatoatomta/ttcsdl/-/blob/master/ThachThoHieu-Module3.sql

-- 1) Thống kê xem trong năm  2020, mỗi một mặt sản phẩm trong mỗi tháng và trong cả năm bán
-- được với số lượng bao nhiêu

select 
ct.MaSP,sp.TenSP,
    sum(case month(CONVERT(date, hd.NgayLap,103)) when 1 then ct.SoLuong
    else 0 end) as N'Tháng 1',
        sum(case month(CONVERT(date, hd.NgayLap,103)) when 2 then ct.SoLuong
    else 0 end) as N'Tháng 2',
        sum(case month(CONVERT(date, hd.NgayLap,103)) when 3 then ct.SoLuong
    else 0 end) as N'Tháng 3',
        sum(case month(CONVERT(date, hd.NgayLap,103)) when 4 then ct.SoLuong
    else 0 end) as N'Tháng 4',
        sum(case month(CONVERT(date, hd.NgayLap,103)) when 5 then ct.SoLuong
    else 0 end) as N'Tháng 5',
        sum(case month(CONVERT(date, hd.NgayLap,103)) when 6 then ct.SoLuong
    else 0 end) as N'Tháng 6',
        sum(case month(CONVERT(date, hd.NgayLap,103)) when 7 then ct.SoLuong
    else 0 end) as N'Tháng 7',
        sum(case month(CONVERT(date, hd.NgayLap,103)) when 8 then ct.SoLuong
    else 0 end) as N'Tháng 8',
        sum(case month(CONVERT(date, hd.NgayLap,103)) when 9 then ct.SoLuong
    else 0 end) as N'Tháng 9',
        sum(case month(CONVERT(date, hd.NgayLap,103)) when 10 then ct.SoLuong
    else 0 end) as N'Tháng 10',
        sum(case month(CONVERT(date, hd.NgayLap,103)) when 11 then ct.SoLuong
    else 0 end) as N'Tháng 11',
        sum(case month(CONVERT(date, hd.NgayLap,103)) when 12 then ct.SoLuong
    else 0 end) as N'Tháng 12',
    sum(ct.SoLuong) as N'Cả năm' from SanPham as sp
inner join CTHoaDon as ct
on sp.MaSP = ct.MaSP
inner join HoaDon as hd
on hd.MaHD = ct.MaHD
where year(CONVERT(datetime, hd.NgayLap,103))= 2020
group by ct.MaSP,sp.TenSP;

--2) Hãy cho biết mỗi một loại sản phẩm bao gồm những sản phẩm nào, 
-- tổng số lượng sản phẩm của mỗi loại sản phẩm và  tổng số lượng của tất cả các sản phẩm hiện có là bao nhiêu? 

select 
    (case     
        when lsp.TenLoaiSP is null then concat(N'Tổng các loại sản phẩm: ', (select count(t.TenLoaiSP) from (select LoaiSanPham.TenLoaiSP from LoaiSanPham,SanPham where LoaiSanPham.MaLoaiSP = SanPham.MaLoaiSP group by LoaiSanPham.TenLoaiSP)  as t) )
        when sp.TenSP is not null then lsp.TenLoaiSP  
       else '---------------------------'
    end) as N'Tên loại sản phẩm',
	(case 
        when sp.TenSP is not null then sp.TenSP
		when lsp.TenLoaiSP is null then concat(N'Tổng các sản phẩm: ',sum(sp.soluong))
        when sp.TenSP is  null and lsp.TenLoaiSP is not null then concat(N'Tổng ', lsp.TenLoaiSP,N' là: ') 
		 else '---------------------------'
    end) as N'Tên sản phẩm' ,
    (case 
		 when sp.TenSP is null and lsp.TenLoaiSP is null then '----------'
		 when sp.TenSP is not null then   CONVERT(nvarchar(50), sum(sp.soluong))  
		 else concat('--', CONVERT(nvarchar(50),sum(sp.soluong)),'--')
    end) as N'Số lượng' 
from LoaiSanPham as lsp
inner join SanPham as sp
on lsp.MaLoaiSP = sp.MaLoaiSP
group BY 
ROLLUP (lsp.TenLoaiSP,sp.TenSP);

-- 3)Hãy cho biết tổng số lượng sản phẩm của mỗi sản phẩm đã có (tổng số lượng sản phẩm
-- hiện có và đã bán). 

select sp.MaSP as N'Mã sản phẩm',sp.TenSP as N'Tên sản phẩm',
    hi.soluong as N'Số lượng ban đầu',
    (case 
		when t.soluong is null then 0
		else t.soluong
    end) as N'Số lượng đã bán',
    (case
		when hi.soluong - t.soluong is null then hi.soluong
        else hi.soluong - t.soluong
    end) as N'Số lượng hàng hiện tại',
	 (case 
		when t.soluong is null then concat(CONVERT(nvarchar,0),N' VND')
		else concat( CONVERT(nvarchar,(LTRIM(STR(sum(t.soluong*t.DonGia),12,0)))), N' VND')
    end) as N'Doanh Thu'
from SanPham as sp
inner join (
	select sp.MaSP as t,sp.TenSP as N'Tên sản phẩm',sp.SoLuong
		from SanPham as sp  ) as hi 
on hi.t= sp.MaSP
inner join (
	select sp.MaSP as t,sp.TenSP as N'Tên sản phẩm',sum(ct.SoLuong) as SoLuong,ct.DonGia
		from SanPham as sp left join CTHoaDon as ct
	on sp.MaSP = ct.MaSP group by sp.MaSP,sp.TenSP,ct.soluong,ct.DonGia) as t 
on t.t= sp.MaSP
group by sp.MaSP,sp.TenSP,hi.soluong,t.soluong,t.DonGia
UNION all select '---------------',N'Tổng',
		(select sum(sp.SoLuong) from SanPham as sp ),
		(select sum(ct.SoLuong) from CTHoaDon as ct ),
		(select sum(sp.SoLuong) from SanPham as sp )-(select sum(ct.SoLuong) from CTHoaDon as ct ),
		(select concat( CONVERT(nvarchar,(LTRIM(STR(sum(ct.SoLuong*ct.DonGia),32,0)))), N' VND') from CTHoaDon as ct )



USE [N02_S4_QuanLyBanHang_QuanAo]
GO

------------------ 3 VIEW ---------------------
--1. Tạo view gồm tất cả nhân viên co giới tính là Nam
create VIEW dsnv_Nam AS
  SELECT *
  FROM dbo.NHANVIEN
  WHERE GioiTinh = 1
  GO
  -- test
  select * from dsnv_Nam;


--2. Tạo 1 view tính tiền lãi của cửa hàng cả 1 năm 2020
  go
create VIEW doanhthu_canam AS
select sum(t.tienlai) as tienlai from (
 select 
	ct.MaSP,
	sp.TenSP,
	sp.SoLuong*sp.DonGia-(sum(ct.SoLuong)*sp.DonGia) as tienlai
from SanPham as sp
inner join CTHoaDon as ct
on sp.MaSP = ct.MaSP
left join HoaDon as hd
on hd.MaHD = ct.MaHD
where year(CONVERT(datetime, hd.NgayLap,103))= 2020
group by ct.MaSP,sp.TenSP,sp.DonGia,sp.SoLuong) as t 
 GO

 -- test
 select * from doanhthu_canam;


 --3.Tạo 1 view tìm ra những những viên ưu tú bán hàng nhiều nhất năm 2020
 go
 create VIEW nhanvien_uutu AS
select hd.MaNV,
	   nv.TenNV
from nhanvien as nv
inner join HoaDon as hd on nv.MaNV = hd.MaNV
where year(CONVERT(datetime, hd.NgayLap,103))= 2020
group by hd.MaNV,nv.TenNV
having count(hd.MaNV) >= all(select count(hd.MaNV) from HoaDon as hd group by hd.MaNV);
 go

 --test
  select * from nhanvien_uutu;


 
 ------------------ 3 proc ---------------------


-- 1)Tạo thủ tục lưu trữ có chức năng thống kê tổng số lượng sản phẩm bán được của một 
-- sản phẩm có mã sản phẩm bất kỳ (mã sản phẩm cần thống kê là tham số của thủ tục).
go
create proc thong_ke_san_pham_theo_ma
	@sp nvarchar(10)
as
begin
	if @sp = (select MaSP from SanPham where MaSP = @sp) 
		begin
			select sp.MaSP as 'san pham',
			(case 
				when sum(ct.soluong) is null then 0
				else sum(ct.soluong) 
			end) as 'Số lượng' 
			from SanPham as sp 
			left join CTHoaDon as ct
			on sp.MaSP = ct.MaSP
			where sp.MaSP = @sp
			group by sp.MaSP;
		end
	else
		print 'Ma san pham khong ton tai';
end ;

 --test
 --trường hợp mã sản phẩm hợp lệ
EXEC thong_ke_san_pham_theo_ma 'SP009'; 
EXEC thong_ke_san_pham_theo_ma 'SP001';
-- trường hợp mã sản phẩm ko hợp lệ
EXEC thong_ke_san_pham_theo_ma 'SP00111';


-- 2.Tạo 1 proc xoá hoá đơn: Xoá cả 2 bảng hoá đơn và chi tiết hoá đơn
go
create PROC xoahoadon
@mahoadon nvarchar(50)
AS 
BEGIN
	if(@mahoadon = (select MaHD from CTHoaDon where MaHD = @mahoadon))
		begin
			DELETE FROM dbo.CTHoaDon WHERE MaHD = @mahoadon
			DELETE FROM dbo.HoaDon WHERE MaHD = @mahoadon	
		end
	else
		begin
			print 'Ma hoa don ' +  @mahoadon  + ' khong ton tai';
		end
END
GO
--test 
EXEC xoahoadon 'HD051'
EXEC xoahoadon 'HD048121'

/* 3. Tạo 1 proc thêm sản phẩm
 - Kiểm tra mã kho có tồn tại hay không
 - Kiểm tra mã loại sản phẩm có tồn tại hay không
 - Tự động tăng mã sản phẩm 
 */
go
create proc insertProduct
	@maloaisanpham nvarchar(50),
	@makho nvarchar(50),
	@tensanpham nvarchar(50),
	@donvitinh nvarchar(50),
	@soluong int,
	@dongia float
	as
	begin
		if @maloaisanpham = (select MaLoaiSP from LoaiSanPham where MaLoaiSP = @maloaisanpham)
		begin
			if @makho = (select MaKho from Kho where MaKho = @makho)
				begin
					begin
						DECLARE @masanpham nvarchar(50)
						IF (SELECT COUNT(MaSP) FROM SanPham) = 0
							SET @masanpham = '0'
						ELSE
								SELECT @masanpham = MAX(RIGHT(MaSP, 3)) FROM SanPham
								SELECT @masanpham = CASE
									WHEN @masanpham >= 0 and @masanpham < 9 THEN 'SP00' + CONVERT(nvarchar, CONVERT(INT, @masanpham) + 1)
									WHEN @masanpham >= 9 THEN 'SP0' + CONVERT(nvarchar, CONVERT(INT, @masanpham) + 1)
								end;
					end
						begin
						if not exists (select MaSP from SanPham where MaSP = @masanpham)
							begin
								INSERT INTO SanPham(MaSP,MaLoaiSP,MaKho,TenSP,DVT,SoLuong,DonGia) VALUES (@masanpham, @maloaisanpham,@makho,@tensanpham,@donvitinh,@soluong,@dongia);
								print 'Them san pham thanh cong';
							end
						else
							print 'Ma san pham da ton tai';
					end
				end
			else
				print 'Ma kho ' +@makho + ' khong ton tai !!'
		end
		else
			print 'Ma loai ' +@maloaisanpham + ' khong ton tai !!'
end;

-- test
EXEC insertProduct N'LSP001', N'K001', N'Ao khoac the thao', N'Chiec',10, 100000
EXEC insertProduct N'LSP001', N'K001', N'Ao khoac ni', N'Chiec',9, 120000
EXEC insertProduct N'LSP002', N'K002', N'Ao khoac den', N'Chiec',20, 160000
EXEC insertProduct N'LSP002', N'K002', N'Ao khoac sang', N'Chiec',30, 180000
EXEC insertProduct N'LSP003', N'K001', N'Quan bo dai', N'Chiec',14, 110000
EXEC insertProduct N'LSP003', N'K001', N'Quan bo den', N'Chiec',16, 90000


------------------ 3 trigger ---------------------

/* 1.Tạo 1 trigger kiểm tra khi insert nhân viên
 - nếu tên nhân viên rỗng: Ko cho insert
 - nếu giới tính rỗng thì tự động gán là giới tính nữ ( giới tính =0)
 */
go
create trigger Kiemtra_NhanVien
   on nhanvien  
	for insert as
    begin
        declare 
				@manhanvien varchar(50),
				@tennhanvien nvarchar(50),
				@gioitinh int,
				@sdt nvarchar(50)
		select @manhanvien =NhanVien.MaNV,@tennhanvien=NhanVien.TenNV,@gioitinh=NhanVien.GioiTinh,@sdt=NhanVien.SDT from 
             NhanVien,inserted where NhanVien.MaNV = inserted.MaNV;
        if(@tennhanvien is null)
			begin
			print N'Ten nhan vien không thể để trống'
			rollback tran 
		end
		if(@gioitinh is null)
			begin
				update NhanVien set GioiTinh = 0 where GioiTinh is null  and NhanVien.MaNV = @manhanvien
			end
    end 
go

-- test
EXEC insertEmployee null, N'0981527073', null
EXEC insertEmployee N'Ngo Thi Ngoc Anh', N'0981527073', null


/*
   2)Tạo 1 trigger kiểm tra khi insert hoặc update vào bảng sản phẩm thì tên sản phẩm là null thì điền là Sản phẩm mới, 
	- Đơn vị tính là null thì tự động ghi là chiếc,
	- đơn giá là null thì tự động ghi là 0
	-số lượng là null hoặc số lượng phải bé hơn 0 không cho insert hoặc update
*/

go
  create TRIGGER Kiem_tra_SanPham
	ON SanPham
	FOR INSERT,update
AS
	DECLARE @masanpham nvarchar(50),
			@tensanpham nvarchar(50),
			@donvitinh nvarchar(50),
			@soluong int,
			@dongia float;
	 select
			@tensanpham =inserted.TenSP,
			@soluong=inserted.SoLuong,
			@donvitinh= inserted.DVT,
			@dongia = inserted.DonGia,
			@masanpham = inserted.MaSP
			from inserted,SanPham;
begin
	if(@tensanpham is null)
		begin
			update SanPham set TenSP = N'Sản phẩm mới' where TenSP is null  and  SanPham.MaSP=@masanpham
		end
	if(@soluong is null or @soluong<0)
		begin
			print N'Số lượng nhập không thể nhỏ hơn 0 hoặc để trống'
			rollback tran 
		end
	if(@donvitinh is null)
		begin
			update SanPham set DVT = N'Chiếc' where DVT is null  and  SanPham.MaSP=@masanpham
		end
	if(@dongia is null)
		begin
			update SanPham set DonGia = 0 where DonGia is null  and  SanPham.MaSP=@masanpham
		end
end

go
-- test 
--  Nhập số lượng bé hơn 0
EXEC insertProduct N'LSP001', N'K001', N'Ao khoac ni', N'Chiec',-9, 120000;
update SanPham set SoLuong =-9 where MaSP ='SP015'

-- Nhập số lượng la null
EXEC insertProduct N'LSP001', N'K001', N'Ao khoac ni', N'Chiec',null, 120000;
update SanPham set SoLuong =null where MaSP ='SP015'

-- Nhập số lượng thoả mãn điều kiện
EXEC insertProduct N'LSP002', N'K002', N'Ao khoac den', N'Chiec',20, 160000;
EXEC insertProduct N'LSP002', N'K002', N'Ao khoac den', null,20, 160000;
EXEC insertProduct N'LSP002', N'K002', null, null,20, null;
update SanPham set SoLuong =9 where MaSP ='SP015'

-- 3) Tạo Trigger in ra câu thông báo mỗi khi thêm 1 Chi tiết Hoá đơn

go
create TRIGGER trg_Insert_Chitiethoadon 
	ON CTHoaDon
	FOR INSERT
AS
	DECLARE 
		@mahoadon nvarchar(50),
		@soluong INT,
		@dongia DECIMAL,
		@TenKH nvarchar(255),
		@masp nvarchar(50),
		@TenSP nvarchar(255)
	SELECT @mahoadon = MaHD, @masp = MaSP, @soluong = SoLuong, @dongia = DonGia FROM Inserted
	SELECT @TenKH = data.TenKH 
	FROM
	(
		SELECT DISTINCT kh.TenKH
		FROM dbo.HOADON hd
		INNER JOIN dbo.KHACHHANG kh ON kh.MaKH = hd.MaKH
		WHERE hd.MaHD = @mahoadon
	) data
	SELECT @TenSP = TenSP FROM dbo.SANPHAM WHERE MaSP = @masp
	PRINT N'Khách hàng: ' + @TenKH + N' đã mua ' + CONVERT(nvarchar(50), @soluong) + ' ' + @TenSP + N' với giá ' + CONVERT(nvarchar,(LTRIM(STR(@dongia,12,0)))) + N' VND' + N'/chiếc '
GO

-- test 
	EXEC insertdata N'NV001',N'SP002', N'Tien mat', N'24/12/2020', N'VND', 2,N'Ngo Thi Ngoc Anh', N'0981527073', null




------------------ 1 Biểu mẫu ---------------------

	-- Biểu mẫu: Lấy ra biểu mẫu theo mã hóa đơn
go
create PROC Bieumau_HoaDon
@mahoadon nvarchar(50) 
AS 
	DECLARE @DonViBan nvarchar(200),
			@MaSoThueBan nvarchar(200),
			@DiaChiBan nvarchar(200),
			@Phone nvarchar(20),
			@Fax nvarchar(20),
			@SoTKBan nvarchar(20),
			@nganhang nvarchar(30),
			@Chinhanh nvarchar(50),
			@NgayHD nvarchar(20),
			@HinhThucTT nvarchar(100),
			@TenKH nvarchar(255),
			@MaSoThue nvarchar(100),
			@email nvarchar(255),
			@SoDT nvarchar(100),
			@STT nvarchar(10),
			@TenSP nvarchar(255),
			@DVT nvarchar(50),
			@SoLuong INT,
			@DonGia DECIMAL(18, 0),
			@ThanhTien DECIMAL(18, 0),
			@tongtienhang DECIMAL(18, 0),
			@TenNV nvarchar(255),
			@Return	nvarchar(4000),
			@mLen int,
			@i int,
			@sNumber nvarchar(4000),
			@mDigit int,
			@mGroup int,
			@mTemp nvarchar(4000),
			@mNumText nvarchar(4000)

	DECLARE Cursor_ThongTin CURSOR SCROLL FOR
		SELECT 
			ROW_NUMBER() OVER (ORDER BY sp.MaSP) STT,
			sp.TenSP, sp.DVT, ct.SoLuong, ct.DonGia, ct.SoLuong * ct.DonGia AS ThanhTien
		FROM dbo.KHACHHANG kh
		INNER JOIN dbo.HOADON hd ON hd.MaKH = kh.MaKH
		INNER JOIN dbo.CTHoaDon ct ON ct.MaHD = hd.MaHD
		INNER JOIN dbo.NHANVIEN nv ON nv.MaNV = hd.MaNV
		INNER JOIN dbo.SANPHAM sp ON sp.MaSP = ct.MaSP
		WHERE hd.MaHD = @mahoadon

	SELECT @DonViBan = N'ATO Shop'
	SELECT @MaSoThue = '160813021999'
	SELECT @DiaChiBan = N'Đình Xuyên - Gia Lâm - Hà Nội'
	SELECT @SoTKBan = '21610000483834'
	SELECT @Phone = '0981537073'
	SELECT @Fax = '000000000000'
	SELECT @nganhang = N'BIDV'
	SELECT @Chinhanh = N'Đống Đa - Hà Nội'
	SELECT @NgayHD = NgayLap, @HinhThucTT = HinhThucThanhToan
	FROM dbo.HOADON WHERE MaHD = @mahoadon
	
	PRINT N'		                                   HÓA ĐƠN BÁN HÀNG'
	PRINT char(13) 
	PRINT N'		                                                            Số hóa đơn: ' +  @mahoadon 
	PRINT N'		                                                            Ngày '+convert(nvarchar(5),day(CONVERT(date, @NgayHD,103)))  + N' Thang ' +convert(nvarchar(5),month(CONVERT(date, @NgayHD,103))) + N' Nam ' +convert(nvarchar(5),year(CONVERT(date, @NgayHD,103)))
	--PRINT char(13) 
	PRINT N'    Mã số thuế: ' + @MaSoThue
	PRINT N'    Đơn vị bán hàng: ' + @DonViBan
	PRINT N'    Địa chỉ: ' + @DiaChiBan
	PRINT N'    Điện thoại: ' + @Phone + N'                                   Fax: ' + @Fax
	PRINT N'    Số tài khoản: ' + @SoTKBan 
	print N'    Tại NH: ' + @nganhang + N'                                             Chi nhánh: ' +@Chinhanh 
	PRINT CHAR(13)
	
	SELECT @TenKH = kh.TenKH, @email = kh.Email, @SoDT = kh.SDT
	FROM dbo.KHACHHANG kh INNER JOIN dbo.HOADON hd ON hd.MaKH = kh.MaKH WHERE hd.MaHD = @mahoadon
	PRINT N'	            Tên khách hàng: ' + @TenKH + N'                     DT: '+@SoDT
	PRINT N'	            Email: ' + @email
	PRINT N'	            Phương thức thanh toán: ' + @HinhThucTT 
	--PRINT CHAR(13)

	SELECT @TenNV = nv.TenNV FROM dbo.NHANVIEN nv INNER JOIN dbo.HOADON hd ON hd.MaNV = nv.MaNV WHERE hd.MaHD = @mahoadon
	SELECT @tongtienhang = 0

	PRINT CHAR(13)
	PRINT '|--------------------------------------------------------------------------------------------------------------------|'
	OPEN Cursor_ThongTin
	PRINT '|  ' + CONVERT(CHAR(7), 'STT') + ' |  ' + CONVERT(NCHAR(30), N'TÊN HÀNG  ') + ' |  ' + CONVERT(NCHAR(15), N'ĐVT') + ' |  ' + CONVERT(NCHAR(10), N'Số lượng') + ' |  ' + CONVERT(NCHAR(20), N'Đơn giá') + ' |  ' + CONVERT(NCHAR(12), N'Thành tiền')+ '|'
	PRINT '|--------------------------------------------------------------------------------------------------------------------|'
	FETCH FIRST FROM Cursor_ThongTin INTO @STT, @TenSP, @DVT, @SoLuong, @DonGia, @ThanhTien
	WHILE @@FETCH_STATUS = 0
		BEGIN
			PRINT '|  ' + CONVERT(CHAR(7), @STT) + ' |  ' + CONVERT(NCHAR(30), @TenSP) + ' |  ' + CONVERT(NCHAR(15), @DVT) + ' |  ' + CONVERT(CHAR(10), @SoLuong) + ' |  ' + CONVERT(CHAR(20), @DonGia) + ' |  ' + CONVERT(CHAR(12), @ThanhTien)+ '|'
			SELECT @tongtienhang = @tongtienhang + @ThanhTien
			FETCH NEXT FROM Cursor_ThongTin INTO @STT, @TenSP, @DVT, @SoLuong, @DonGia, @ThanhTien
		END	
	CLOSE Cursor_ThongTin
	PRINT '|--------------------------------------------------------------------------------------------------------------------|'
	DEALLOCATE Cursor_ThongTin

		PRINT '|  ' + CONVERT(NCHAR(27), '') + CONVERT(NCHAR(14),  N'Tổng tiền: ')+' |  ' + CONVERT(CHAR(25), '') + convert(CHAR(20), concat(CONVERT(nvarchar(50), @tongtienhang),' VND'))   + CONVERT(CHAR(23), '')  +' |  ' 
		--	PRINT CHAR(13)

	PRINT '|--------------------------------------------------------------------------------------------------------------------|'
	-- xu ly in so tien thanh chu

		SELECT @sNumber=LTRIM(STR(@ThanhTien))
		SELECT @mLen = Len(@sNumber)

		SELECT @i=1
		SELECT @mTemp=''

		WHILE @i <= @mLen
		BEGIN

		SELECT @mDigit=SUBSTRING(@sNumber, @i, 1)

		IF @mDigit=0 SELECT @mNumText=N'không'
		ELSE
		BEGIN
		IF @mDigit=1 SELECT @mNumText=N'một'
		ELSE
		IF @mDigit=2 SELECT @mNumText=N'hai'
		ELSE
		IF @mDigit=3 SELECT @mNumText=N'ba'
		ELSE
		IF @mDigit=4 SELECT @mNumText=N'bốn'
		ELSE
		IF @mDigit=5 SELECT @mNumText=N'năm'
		ELSE
		IF @mDigit=6 SELECT @mNumText=N'sáu'
		ELSE
		IF @mDigit=7 SELECT @mNumText=N'bảy'
		ELSE
		IF @mDigit=8 SELECT @mNumText=N'tám'
		ELSE
		IF @mDigit=9 SELECT @mNumText=N'chín'
		END

		SELECT @mTemp = @mTemp + ' ' + @mNumText

		IF (@mLen = @i) BREAK

		Select @mGroup=(@mLen - @i) % 9

		IF @mGroup=0
		BEGIN
		SELECT @mTemp = @mTemp + N' tỷ'

		If SUBSTRING(@sNumber, @i + 1, 3) = N'000' 
		SELECT @i = @i + 3

		If SUBSTRING(@sNumber, @i + 1, 3) = N'000' 
		SELECT @i = @i + 3

		If SUBSTRING(@sNumber, @i + 1, 3) = N'000' 
		SELECT @i = @i + 3
		END 
		ELSE
		IF @mGroup=6
		BEGIN
		SELECT @mTemp = @mTemp + N' triệu'

		If SUBSTRING(@sNumber, @i + 1, 3) = N'000' 
		SELECT @i = @i + 3

		If SUBSTRING(@sNumber, @i + 1, 3) = N'000' 
		SELECT @i = @i + 3
		END
		ELSE
		IF @mGroup=3
		BEGIN
		SELECT @mTemp = @mTemp + N' nghìn'

		If SUBSTRING(@sNumber, @i + 1, 3) = N'000' 
		SELECT @i = @i + 3

		END
		ELSE
		BEGIN

		Select @mGroup=(@mLen - @i) % 3

		IF @mGroup=2	
		SELECT @mTemp = @mTemp + N' trăm'
		ELSE
		IF @mGroup=1
		SELECT @mTemp = @mTemp + N' mươi'	
		END


		SELECT @i=@i+1
		END

		--'Loại bỏ trường hợp x00

		SELECT @mTemp = Replace(@mTemp, N'không mươi không', '')

		--'Loại bỏ trường hợp 00x

		SELECT @mTemp = Replace(@mTemp, N'không mươi ', N'linh ')

		--'Loại bỏ trường hợp x0, x>=2

		SELECT @mTemp = Replace(@mTemp, N'mươi không', N'mươi')

		--'Fix trường hợp 10

		SELECT @mTemp = Replace(@mTemp, N'một mươi', N'mười')

		--'Fix trường hợp x4, x>=2

		SELECT @mTemp = Replace(@mTemp, N'mươi bốn', N'mươi tư')

		--'Fix trường hợp x04 

		SELECT @mTemp = Replace(@mTemp, N'linh bốn', N'linh tư')

		--'Fix trường hợp x5, x>=2

		SELECT @mTemp = Replace(@mTemp, N'mươi năm', N'mươi nhăm')

		--'Fix trường hợp x1, x>=2

		SELECT @mTemp = Replace(@mTemp, N'mươi một', N'mươi mốt')

		--'Fix trường hợp x15

		SELECT @mTemp = Replace(@mTemp, N'mười năm', N'mười lăm')

		--'Bỏ ký tự space

		SELECT @mTemp = LTrim(@mTemp)

		--'Ucase ký tự đầu tiên

		SELECT @Return=UPPER(Left(@mTemp, 1)) + SUBSTRING(@mTemp,2, 4000) + N' Việt Nam đồng'
		PRINT char(13)
		PRINT N'Tổng tiền: ' + CONVERT(nvarchar(20), @tongtienhang) + ' VND'
		PRINT N'Cộng  thành tiền (Viết bằng chữ): ' + CONVERT(nvarchar(50), @Return)

	PRINT char(13)
	PRINT '                       '+ CONVERT(NCHAR(30), N'Khách hàng') + '                       ' + CONVERT(NCHAR(30), N'Nhân viên') 
	PRINT char(13)
	PRINT '                     '+ CONVERT(NCHAR(30), @TenKH) + '                      ' + CONVERT(NCHAR(30), @TenNV)

GO

EXEC Bieumau_HoaDon 'HD001'






